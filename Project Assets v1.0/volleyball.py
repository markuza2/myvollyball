"""
Zone 1: Program Initialization.
"""
# Required Imports.
import numpy as np # Use 'pip install numpy' if not installed with your Python.
import numpy.matlib
import matplotlib.pyplot as plt # Use 'pip install matplotlib' if not installed with your Python.
import math as mt
import cv2
import json
from sklearn.externals.joblib import dump, load
from tensorflow.keras.layers import *
from tensorflow.keras.models import *
from tensorflow.keras.models import load_model

# Flags for different options while running the game.
ShowBoard = False # Showing Board View (called 2d view in the code).
ShowPlayerView = True # Showing view as seen by our player (the view given to the student artificial player).
SavePlayerView = False # Saving images of views as seen by player (saved to ./plots directory). These files should be used for training.
UseTrainedModel = False # Choosing if we use the trained model or a random player for the student artificial player.

# Starting the game with random positions for the players and the ball.
def initiate_game(start_team, plyrs_loc, new_match):
    if new_match:
        plyrs_loc[0:N_plyrs * 2, 0] = np.random.randint(mt.floor(fld_w * 0.05), mt.floor(fld_w * 0.95),
                                                        size=[1, N_plyrs * 2])
        plyrs_loc[0:N_plyrs, 1] = np.random.randint(mt.floor(fld_l * 0.05), mt.floor(fld_l * 0.45), size=[1, N_plyrs])
        plyrs_loc[N_plyrs:N_plyrs * 2, 1] = np.random.randint(mt.floor(fld_l * 0.55), mt.floor(fld_l * 0.95),
                                                              [1, N_plyrs])

    ball_loc = np.zeros((1, 3))
    ball_loc[0, 0] = np.random.randint(mt.floor(0.1 * fld_w), mt.floor(0.9 * fld_w))
    ball_loc[0, 1] = np.random.randint(mt.floor(((start_team - 1) * 0.5 + 0.1) * fld_l),
                                       mt.floor(((start_team - 1) * 0.5 + 0.2) * fld_l))
    ball_loc[0, 2] = plyr_h

    ball_vel = np.zeros((1, 3))
    v0 = hit_pwr_max * 0.6 * pxl_per_meter / time_unit_per_sec  # absolute value of the ball's velocity
    alpha = 45 * (mt.pi / 180)  # degrees over the horizon of the velocity
    ball_vel[0, 0] = 0
    ball_vel[0, 1] = v0 * mt.cos(alpha) * (-2 * start_team + 3)
    ball_vel[0, 2] = v0 * mt.sin(alpha)

    t_a = g / 2
    t_b = ball_vel[0, 2]
    t_c = ball_loc[0, 2] - plyr_h
    temp = t_b ** 2 - 4 * t_a * t_c
    if temp < 0:
        temp = 0
    t1 = (-t_b + mt.sqrt(temp)) / (2 * t_a)
    t2 = (-t_b - mt.sqrt(temp)) / (2 * t_a)
    t = max(t1, t2)
    xf = ball_loc[0, 0] + ball_vel[0, 0] * t
    yf = ball_loc[0, 1] + ball_vel[0, 1] * t

    return [plyrs_loc, ball_loc, ball_vel, xf, yf]


"""
Zone 2: Student Zone. Start writing your answer here.
"""

# Below is an example of a simplified solution.

VirtualBallFollower = (0,0,0) # Global variable used to display the predicted ball position on the board.

# Size of the images that our model was trained by.
width = 400
height = 200

# Getting the trained model.
model = load_model('complete_model.h5') # Use the model you trained on colab and dowloaded to your disk, and place it in the directory of this program.

# Your artificial voleyball player.
def student_player(image, ball_in_view):
    global VirtualBallFollower
    if ball_in_view:
        if UseTrainedModel:
            image = cv2.resize(image,(width,height)) # Resizing to the same tensor size as used when learning (1,width,height,1).
            image = np.expand_dims(image,-1)  
            image = np.expand_dims(image, axis=0) 

            # Doing prediction using the trained model.
            prediction = model.predict(image)
            ball_distance, ball_theta, ball_phi = prediction[0]
            VirtualBallFollower = prediction[0] # Provision for displaying the predicted vector to ball.
            ball_theta = ball_theta/100.0 # Used for normalization as in the training.
            ball_phi = ball_phi/100.0 # User for normalization as in the training.
            
            # Call your unsupervised machine learning part here and calculate your game step and hit parameters...

            # Example: Random player (same as below).
            move = np.random.randint(1, 5, (1, 1)) - 1
            phi_range = 40
            phi_offset = 90
            hit_phi = np.random.random(1) * phi_range * 2 * pi / 180 + phi_offset * pi / 180 - phi_range * pi / 180
            hit_theta = pi / 4
            pwr_offset = hit_pwr_max * 0.7
            pwr_range = hit_pwr_max * 0.3
            hit_pwr = np.random.random(1) * pwr_range + pwr_offset - pwr_range/2
        else:
            # Example: Random player (same as above).
            move = np.random.randint(1, 5, (1, 1)) - 1
            phi_range = 40
            phi_offset = 90
            hit_phi = np.random.random(1) * phi_range * 2 * pi / 180 + phi_offset * pi / 180 - phi_range * pi / 180
            hit_theta = pi / 4
            pwr_offset = hit_pwr_max * 0.7
            pwr_range = hit_pwr_max * 0.3
            hit_pwr = np.random.random(1) * pwr_range + pwr_offset - pwr_range/2
    else: # Doing something if the ball is not in the view.
        # For the example we're not really doing anything...
        move = np.random.randint(1, 5, (1, 1)) - 2
        hit_pwr = 0
        hit_theta = 0
        hit_phi = 0

    # This is your answer.
    return [move, hit_pwr, hit_theta, hit_phi]


"""
Zone 3: The World.
"""

# Team 1 players except for the student player
def team1_turn(image, ball_loc, ball_vel, plyr_loc, p):
    move = 0
    dist_vec = distances(1)
    if (dist_vec == np.min(dist_vec))[0, p]:
        move_phi = mt.atan2(yf - plyr_loc[1], xf - plyr_loc[0])
        if move_phi >= 0:
            if (move_phi < 3 * mt.pi / 4) and (move_phi > mt.pi / 4):
                move = 1  # forward
            elif move_phi < mt.pi / 4:
                move = 3  # right
            else:
                move = 4  # left
        else:
            if (move_phi > (-3 * mt.pi / 4)) and (move_phi < (-mt.pi / 4)):
                move = 2  # back
            elif move_phi > (-mt.pi / 4):
                move = 3  # right
            else:
                move = 4  # left

    extra = 0
    next_x = np.random.random(1) * fld_w * (1+extra*2) - extra * fld_w
    next_y = np.random.random(1) * fld_l * 0.5 * (1+extra*2) + fld_l * 0.5 - extra * fld_l
    L = dist(plyr_loc, [next_x, next_y]) / pxl_per_meter
    ball_z = ball_loc[0, 2] / pxl_per_meter
    hit_phi = mt.atan2(next_y - plyr_loc[1], next_x - plyr_loc[0])
    hit_theta = pi / 4
    hit_pwr = mt.sqrt(2 * 5 * L**2 / (ball_z + L))

    return [move, hit_pwr, hit_theta, hit_phi]

# Team 2 players
def team2_turn(image, ball_loc, ball_vel, plyr_loc, p):
    move = 0
    dist_vec = distances(2)
    if (dist_vec == np.min(dist_vec))[0, p-N_plyrs]:
        move_phi = mt.atan2(yf - plyr_loc[1], xf - plyr_loc[0])
        if move_phi >= 0:
            if (move_phi < 3 * mt.pi / 4) and (move_phi > mt.pi / 4):
                move = 1  # forward
            elif move_phi < mt.pi / 4:
                move = 3  # right
            else:
                move = 4  # left
        else:
            if (move_phi > (-3 * mt.pi / 4)) and (move_phi < (-mt.pi / 4)):
                move = 2  # back
            elif move_phi > (-mt.pi / 4):
                move = 3  # right
            else:
                move = 4  # left

        temp_vec = np.zeros((4, 1))
        temp_vec[move - 1] = 1
        temp_vec = np.matmul(rotate_matrix, temp_vec)
        idx = np.where(temp_vec == 1)
        move = idx[0][0] + 1

    extra = 0
    next_x = np.random.random(1) * fld_w * (1 + extra * 2) - extra * fld_w
    next_y = np.random.random(1) * fld_l * 0.5 * (1 + extra * 2) - extra * fld_l
    L = dist(plyr_loc, [next_x, next_y]) / pxl_per_meter
    ball_z = ball_loc[0, 2] / pxl_per_meter
    hit_phi = mt.atan2(next_y - plyr_loc[1], next_x - plyr_loc[0]) + pi
    hit_theta = pi / 4
    hit_pwr = mt.sqrt(2 * 5 * L ** 2 / (ball_z + L))

    return [move, hit_pwr, hit_theta, hit_phi]


def advance_plyrs(plyrs_loc, move):
    plyrs_to_move = np.ones((1, N_plyrs * 2))  # the player that need to be moved
    plyrs_collide = np.zeros((N_plyrs * 2, 1))  # players that will collide
    moves_explct = np.zeros((N_plyrs * 2, 2))  # an array that has the actual
    # addition to the player location
    for j in range(0, N_plyrs):  # first team
        if move[j] == 1:
            moves_explct[j, 1] = 1
        elif move[j] == 2:
            moves_explct[j, 1] = -1
        elif move[j] == 3:
            moves_explct[j, 0] = 1
        elif move[j] == 4:
            moves_explct[j, 0] = -1

    for j in range(N_plyrs, N_plyrs * 2):  # second team
        if move[j] == 1:
            moves_explct[j, 1] = -1
        elif move[j] == 2:
            moves_explct[j, 1] = 1
        elif move[j] == 3:
            moves_explct[j, 0] = -1
        elif move[j] == 4:
            moves_explct[j, 0] = 1

    # checks going out of field boundaries
    for j in range(0, N_plyrs):  # first team
        if plyrs_loc[j, 0] + moves_explct[j, 0] <= 0:
            plyrs_to_move[0][j] = 0
        elif plyrs_loc[j, 0] + moves_explct[j, 0] >= fld_w:
            plyrs_to_move[0][j] = 0
        elif plyrs_loc[j, 1] + moves_explct[j, 1] >= fld_l / 2:
            plyrs_to_move[0][j] = 0
        elif plyrs_loc[j, 1] + moves_explct[j, 1] <= 0:
            plyrs_to_move[0][j] = 0

    for j in range(N_plyrs, N_plyrs * 2):  # second team
        if plyrs_loc[j, 0] + moves_explct[j, 0] <= 0:
            plyrs_to_move[0][j] = 0
        elif plyrs_loc[j, 0] + moves_explct[j, 0] >= fld_w:
            plyrs_to_move[0][j] = 0
        elif plyrs_loc[j, 1] + moves_explct[j, 1] <= fld_l / 2:
            plyrs_to_move[0][j] = 0
        elif plyrs_loc[j, 1] + moves_explct[j, 1] >= fld_l:
            plyrs_to_move[0][j] = 0

    # checks collision
    for j in range(0, N_plyrs):  # first team
        temp_loc = plyrs_loc[j, :] + moves_explct[j, :]  # the player's next planned location
        for k in range(0, N_plyrs):
            if k == j:
                continue
            if np.array_equal(temp_loc, plyrs_loc[k, :]):
                plyrs_to_move[0][j] = 0

                # if a player is trying to move to a point where there is
                # another player, he will bounce back with an amplitude of
                # "bounce_back" (global)
                temp_loc_new = plyrs_loc[j, :] - bounce_back_amp * moves_explct[j, :]
                if (temp_loc_new[0] > 0) and (temp_loc_new[0] < fld_w) and \
                        (temp_loc_new[1] > 0) and (temp_loc_new[1] < fld_l / 2):
                    # if the collision won't send the player outside the field
                    plyrs_loc[j, :] = temp_loc_new
                plyrs_collide[j] = 1

    for j in range(N_plyrs, N_plyrs * 2):  # second team
        temp_loc = plyrs_loc[j, :] + moves_explct[j, :]  # the player's next planned location
        for k in range(N_plyrs, N_plyrs * 2):
            if k == j:
                continue
            if np.array_equal(temp_loc, plyrs_loc[k, :]):
                plyrs_to_move[0][j] = 0

                # if a player is trying to move to a point where there is
                # another player, he will bounce back with an amplitude of
                # "bounce_back" (global)
                temp_loc_new = plyrs_loc[j, :] - bounce_back_amp * moves_explct[j, :]
                if (temp_loc_new[0] > 0) and (temp_loc_new[0] < fld_w) and (temp_loc_new[1] > fld_l / 2) and \
                        (temp_loc_new[1] < fld_l):
                    # if the collision won't send the player outside the field
                    plyrs_loc[j, :] = temp_loc_new

                plyrs_collide[j] = 1
    ptm = np.where(plyrs_to_move[0] == 1)
    if ptm[0].shape[0]:
        for j in range(0, len(ptm[0])):
            plyrs_loc[ptm[0][j], :] = plyrs_loc[ptm[0][j], :] + moves_explct[ptm[0][j], :]
            # plyrs_loc[ptm[0][j], :] = plyrs_loc[ptm[0][j], :] + moves_explct[ptm[0][j], :]
    for j in range(N_plyrs * 2):
        for k in range(N_plyrs * 2):
            if j == k:
                continue
            if np.all(plyrs_loc[j, :] == plyrs_loc[k, :]):
                moveit = -np.random.randint(1, 3, size=[1, 2]) * 2 + 3
                plyr_loc_temp = plyrs_loc[j, :] - np.random.randint(1, 3, size=[1, 2]) * 2 + 3
                while (plyr_loc_temp[0, 0] <= 0) or (plyr_loc_temp[0, 0] >= fld_w) or (
                        ((plyr_loc_temp[0, 1] <= 0) or (plyr_loc_temp[0, 1] >= fld_l / 2)) and (j < N_plyrs)) or (
                        ((plyr_loc_temp[0, 1] <= fld_l / 2) or (plyr_loc_temp[0, 1] >= fld_l)) and (j >= N_plyrs)):
                    moveit = -np.random.randint(1, 3, size=[1, 2]) * 2 + 3
                    plyr_loc_temp = plyrs_loc[j, :] - np.random.randint(1, 3, size=[1, 2]) * 2 + 3
                plyrs_loc[j, :] = plyrs_loc[j, :] + moveit
    return [plyrs_loc, plyrs_collide]


def advance_ball(ball_loc, ball_vel, plyrs_loc, hit_pwr, hit_theta, hit_phi, who_hit, xf, yf):
    hit_pwr = hit_pwr * pxl_per_meter / time_unit_per_sec  # adjust to pxl/time_unit
    ball_loc = ball_loc + ball_vel
    ball_vel[0, 2] = ball_vel[0, 2] + g

    score = 0
    next_game = 0
    reason = ''
    if ball_loc[0, 2] <= 0:  # if the ball hits the ground

        if ball_inside(ball_loc[0, 0:2]):  # if the ball fell inside the field
            if ball_loc[0, 1] < fld_l / 2:  # if the ball hit the first half of the field
                if who_hit < N_plyrs:  # if the player hit its own part
                    score = np.abs(fld_l/2 - ball_loc[0, 1]) * score_ball_hits_own_side
                    next_game = 2
                    reason = "fell in hitter's side of the field"
                elif who_hit < N_plyrs * 2:
                    # squared distance of the fall point from each player
                    fall_dist_2_plyrs = np.sum(
                        (plyrs_loc[0:N_plyrs, 0:2] - np.matlib.repmat(ball_loc[0, 0:2], N_plyrs, 1)) ** 2, 1)
                    shortest_dist = np.min(fall_dist_2_plyrs)
                    score = max([np.sqrt(shortest_dist) * score_success_multiplier, score_success_team_member*2])

                    fall_2_lossing_plyrs = distances_4_score(1)
                    score_count[0:N_plyrs] += score_punish_losing_point*np.divide(fld_w, fall_2_lossing_plyrs).reshape(-1, 1)
                    reason = "scored with "+str(score)+" points"
                    next_game = 2
                else:
                    next_game = np.random.randint(1, 3)
            elif ball_loc[0, 1] > fld_l / 2:  # if the ball hit the second half of the field
                if who_hit < N_plyrs:
                    # squared distance of the fall point from each player
                    fall_dist_2_plyrs = \
                        np.sum((plyrs_loc[N_plyrs: N_plyrs * 2, 0:2] - np.matlib.repmat(ball_loc[0, 0:2], N_plyrs,
                                                                                        1)) ** 2, 1)
                    shortest_dist = np.min(fall_dist_2_plyrs)
                    score = max([np.sqrt(shortest_dist) * score_success_multiplier, score_success_team_member*2])

                    fall_2_lossing_plyrs = distances_4_score(2)
                    score_count[N_plyrs:N_plyrs*2] += score_punish_losing_point * np.divide(fld_w, fall_2_lossing_plyrs).reshape(-1, 1)

                    next_game = 1
                elif who_hit < N_plyrs * 2:
                    score = np.abs(fld_l/2 - ball_loc[0, 1]) * score_ball_hits_own_side
                    next_game = 1
                else:
                    next_game = np.random.randint(1, 3)
            else:  # if the ball hit the net itself
                score = 0
                if who_hit == (N_plyrs * 2):
                    next_game = np.random.randint(1, 3)
                else:
                    next_game = np.floor((who_hit) / N_plyrs) + 1

    # if the ball hits the net or under it
    elif (ball_loc[0, 1] < fld_l / 2 + net_thk) and (ball_loc[0, 1] > fld_l / 2 - net_thk) and (ball_loc[0, 2] < net_h):
        score = score_ball_hits_net
        reason='hit net'
        if who_hit == (N_plyrs * 2):
            next_game = np.random.randint(1, 3)
        else:
            next_game = -mt.floor((who_hit) / N_plyrs) + 2

    # if the ball hasn't hit the floor (but has a chance of getting to a player)
    elif (ball_loc[0, 2] > 0.8 * (top_plyr_hit * plyr_h - ball_r)) and (ball_loc[0, 2] < 1.2 * (plyr_h + ball_r)):
        plyr_points_size = 5
        plyr_can_hit = np.zeros((N_plyrs * 2, 1))  # an array that states which players can hit the ball

        for p in range(0, N_plyrs * 2):
            plyr_ball_dist_arr = np.column_stack((plyrs_loc[p, 0] * np.ones((plyr_points_size, 1)),
                                                  plyrs_loc[p, 1] * np.ones((plyr_points_size, 1)),
                                                  np.linspace((1 - top_plyr_hit) * plyr_h + 1, plyr_h,
                                                              plyr_points_size).reshape(-1,
                                                                                        1)))  # array of points for the upper part of the player
            plyr_ball_dist_arr = plyr_ball_dist_arr - np.matlib.repmat(ball_loc, plyr_points_size, 1)
            plyr_ball_dist_arr = np.sum(plyr_ball_dist_arr ** 2, axis=1)  # distance of the ball from each point squared
            if min(plyr_ball_dist_arr) < ball_r ** 2:  # if the ball touches the upper part of the player
                plyr_can_hit[p] = 1

        if sum(plyr_can_hit) > 0:
            plyr_can_hit = np.where(plyr_can_hit == 1)[0]  # which players can hit
            who_hit = plyr_can_hit[
                np.random.randint(0, len(plyr_can_hit))]  # chooses who will hit out of the ones that can

            if hit_pwr[who_hit] <= 0:
                hit_pwr[who_hit] = np.random.random((1, 1)) * hit_pwr_max * 0.8 + hit_pwr_max * 0.2
            elif hit_pwr[who_hit] > hit_pwr_max:
                hit_pwr[who_hit] = hit_pwr_max
            if (hit_theta[who_hit] <= 0) or (hit_theta[who_hit] > pi):
                hit_theta[who_hit] = np.random.random((1, 1)) * 10 * pi / 180 + 40 * pi / 180
            if (hit_phi[who_hit] < 0) or (hit_phi[who_hit] > pi):
                hit_phi[who_hit] = np.random.random((1, 1)) * 40 * pi / 180 + pi / 2
            if who_hit >= N_plyrs:
                hit_phi = hit_phi + pi
            ball_vel = np.array((hit_pwr[who_hit] * mt.sin(hit_theta[who_hit]) * mt.cos(hit_phi[who_hit]),
                                 hit_pwr[who_hit] * mt.sin(hit_theta[who_hit]) * mt.sin(hit_phi[who_hit]),
                                 hit_pwr[who_hit] * mt.cos(hit_theta[who_hit]))).reshape(1, -1)
            ball_loc = ball_loc + ball_vel  # np.array((ball_loc[0, 0] + ball_vel[0, 0] * 2, ball_loc[0, 1] + ball_vel[0, 1] * 2,  ball_loc[0, 2] + ball_vel[0, 2] * 2))
            ball_loc = ball_loc + ball_vel

            t_a = g / 2
            t_b = ball_vel[0, 2]
            t_c = ball_loc[0, 2] - plyr_h
            temp = t_b ** 2 - 4 * t_a * t_c
            if temp < 0:
                temp = 0
            t1 = (-t_b + mt.sqrt(temp)) / (2 * t_a)
            t2 = (-t_b - mt.sqrt(temp)) / (2 * t_a)
            t = max(t1, t2)
            xf = ball_loc[0, 0] + ball_vel[0, 0] * t
            yf = ball_loc[0, 1] + ball_vel[0, 1] * t


    # if the ball fell out side the field
    if (ball_loc[0, 0] > fld_w) or (ball_loc[0, 0] < 0) or (ball_loc[0, 1] > fld_l) or (ball_loc[0, 1] < 0):
        score = score_ball_hits_out*ball_loc[0, 2]
        reason = 'fell outside the field'
        if who_hit == (N_plyrs * 2):
            next_game = np.random.randint(1, 3)
        else:
            next_game = -mt.floor((who_hit) / N_plyrs) + 2
    print ('reason=',reason)
    return [ball_loc, ball_vel, who_hit, next_game, score, xf, yf, reason]


def ball_inside(ball_loc):
    if (ball_loc[0] < fld_w * 0.99) and (ball_loc[0] > 0.01) and (ball_loc[1] < fld_l * 0.99) and (ball_loc[1] > 0.01):
        fell_inside = 1
    else:
        fell_inside = 0
    return fell_inside


def make_2d_image(plyrs_loc, ball_loc, plyr_num):
    image2d = np.zeros((image2d_h, image2d_w))
    ball_plyr_dist = mt.sqrt(np.sum(([plyrs_loc[plyr_num - 1, 0], plyrs_loc[plyr_num - 1, 1],
                                      plyr_h] - ball_loc) ** 2))  # distance between ball and player
    ball_theta = mt.acos((ball_loc[0, 2] - plyr_h) / ball_plyr_dist)
    ball_theta_pxl = int(np.floor((ball_theta - theta_min) * image2d_h / (theta_max - theta_min)))
    ball_phi = mt.atan2((ball_loc[0, 1] - plyrs_loc[plyr_num - 1, 1]), (ball_loc[0, 0] - plyrs_loc[plyr_num - 1, 0]))
    if plyr_num > N_plyrs:  # flips the angle of the ball for players from the 2nd team
        ball_phi = ball_phi + mt.pi
    ball_phi_pxl = int(np.floor((ball_phi - phi_min) * image2d_w / (phi_max - phi_min)))
    ball_angle_rad = mt.atan(ball_r / mt.sqrt(np.sum((ball_loc[0, 0:2] - plyrs_loc[plyr_num - 1, :]) ** 2) + (
                ball_loc[0, 2] - plyr_h) ** 2))  # the radius of the ball on the 2d image (radians)
    ball_angle_rad_pxl = int(
        np.floor(ball_angle_rad * image2d_w / (phi_max - phi_min)))  # the radius of the ball on the 2d image (pixels)
    # if ((ball_phi+ball_angle_rad >= phi_min) or ((ball_phi-ball_angle_rad <= phi_max) and (ball_phi-ball_angle_rad > 0))) \
    #         and ((ball_theta+ball_angle_rad >= theta_min) or ((ball_theta-ball_angle_rad <= theta_max) and (ball_theta-ball_angle_rad > 0))):
    ball_in_view = False
    if ((ball_phi + ball_angle_rad >= phi_min) and (
            (ball_phi - ball_angle_rad <= phi_max) and (ball_phi - ball_angle_rad > 0))) \
            and ((ball_theta + ball_angle_rad >= theta_min) and (
            (ball_theta - ball_angle_rad <= theta_max) and (ball_theta - ball_angle_rad > 0))):
        ball_in_view = True
        for j in range(np.max([ball_theta_pxl - ball_angle_rad_pxl, 1]),
                       np.min([ball_theta_pxl + ball_angle_rad_pxl, image2d_h])):
            k = 0
            while (k ** 2 + (j - ball_theta_pxl) ** 2) < (ball_angle_rad_pxl ** 2):
                k = k + 1
            if ball_phi_pxl + k > 0:
                k_range = range(np.max([ball_phi_pxl - k, 1]), np.min([ball_phi_pxl + k, image2d_w]))
                image2d[j, k_range] = 1
    for p in range(0, N_plyrs * 2):
        if p == plyr_num - 1:
            continue
        plyr_phi = mt.atan2((plyrs_loc[p, 1] - plyrs_loc[plyr_num - 1, 1]),
                            (plyrs_loc[p, 0] - plyrs_loc[plyr_num - 1, 0]))
        if plyr_num > N_plyrs:  # flips the angle of the seen player for observer from the 2nd team
            plyr_phi = plyr_phi + mt.pi
        plyr_phi_pxl = np.floor((plyr_phi - phi_min) * image2d_w / (phi_max - phi_min))
        plyr_2_plyr_dist_head = mt.sqrt(np.sum((plyrs_loc[p, :] - plyrs_loc[plyr_num - 1, :]) ** 2))
        plyr_2_plyr_dist_floor = mt.sqrt(np.sum((plyrs_loc[p, :] - plyrs_loc[plyr_num - 1, :]) ** 2 + plyr_h ** 2))
        plyr_phi_rad = mt.atan(
            0.5 * (plyr_w - 1) / (plyr_2_plyr_dist_head + 0.0001))  # the radius of the player on the 2d image
        plyr_phi_rad_pxl = int(np.min([np.floor(plyr_phi_rad * image2d_w / (phi_max - phi_min)), plyr_rad_pxl_limit]))
        plyr_phi_min = plyr_phi - plyr_phi_rad
        plyr_phi_max = plyr_phi + plyr_phi_rad
        if (plyr_phi_max >= phi_min) and ((plyr_phi_min <= phi_max) and (plyr_phi_min > 0)):
            plyr_phi_min_pxl = int(np.max([np.floor((plyr_phi_min - phi_min) * image2d_w / (phi_max - phi_min)), 1]))
            plyr_phi_max_pxl = int(
                np.min([np.floor((plyr_phi_max - phi_min) * image2d_w / (phi_max - phi_min)), image2d_w]))
            plyr_theta_max = mt.acos(-plyr_h / plyr_2_plyr_dist_floor)
            plyr_theta_min = mt.pi / 2  # all players are the same height :)
            if (plyr_theta_max > theta_min) and ((plyr_theta_min < theta_max) and (plyr_theta_min > 0)):
                plyr_theta_min_pxl = int(
                    np.max([np.floor((plyr_theta_min - theta_min) * image2d_h / (theta_max - theta_min)), 1]))
                plyr_theta_max_pxl = int(
                    np.min([np.floor((plyr_theta_max - theta_min) * image2d_h / (theta_max - theta_min)), image2d_h]))
                image2d[plyr_theta_min_pxl: plyr_theta_max_pxl, plyr_phi_min_pxl: plyr_phi_max_pxl] = 1
    # net
    net_phi_max = mt.atan2(fld_l / 2 - plyrs_loc[plyr_num - 1, 1], -plyrs_loc[plyr_num - 1, 0])
    net_phi_min = mt.atan2(fld_l / 2 - plyrs_loc[plyr_num - 1, 1], fld_w - plyrs_loc[plyr_num - 1, 0])

    net_phi_min_pxl = int(np.floor((net_phi_min - phi_min) * image2d_w / (phi_max - phi_min)))
    net_phi_max_pxl = int(np.floor((net_phi_max - phi_min) * image2d_w / (phi_max - phi_min)))

    net_phi_arr_pxl = np.floor(np.linspace(net_phi_min_pxl, net_phi_max_pxl, net_columns_N))
    net_phi_arr_pxl = np.array((net_phi_arr_pxl, net_phi_arr_pxl + 1, net_phi_arr_pxl - 2))

    net_phi_min_disp = mt.atan2(fld_l / 2 - plyrs_loc[plyr_num - 1, 1], np.min(
        [fld_w, plyrs_loc[plyr_num - 1, 0] + (fld_l / 2 - plyrs_loc[plyr_num - 1, 1]) / mt.tan(phi_min)]) - plyrs_loc[
                                    plyr_num - 1, 0])
    net_phi_max_disp = mt.atan2(fld_l / 2 - plyrs_loc[plyr_num - 1, 1], np.max(
        [0, plyrs_loc[plyr_num - 1, 0] + (fld_l / 2 - plyrs_loc[plyr_num - 1, 1]) / mt.tan(phi_max)]) - plyrs_loc[
                                    plyr_num - 1, 0])
    net_phi_min_pxl_disp = int(np.max((np.floor((net_phi_min_disp - phi_min) * image2d_w / (phi_max - phi_min)), 1)))
    net_phi_max_pxl_disp = int(
        np.min((np.floor((net_phi_max_disp - phi_min) * image2d_w / (phi_max - phi_min)), image2d_w)))

    for x in range(net_phi_min_pxl_disp, net_phi_max_pxl_disp):
        phi = x * (phi_max - phi_min) / image2d_w + phi_min
        if phi == mt.pi / 2:
            x_real_pos = plyrs_loc[plyr_num - 1, 0]
        else:
            x_real_pos = plyrs_loc[plyr_num - 1, 0] + (fld_l / 2 - plyrs_loc[plyr_num - 1, 1]) / mt.tan(phi)

        r_top = mt.sqrt(
            (fld_l / 2 - plyrs_loc[plyr_num - 1, 1]) ** 2 + (x_real_pos - plyrs_loc[plyr_num - 1, 0]) ** 2 + (
                        net_h - plyr_h) ** 2)
        net_theta_top = mt.acos((net_h - plyr_h) / r_top)
        net_theta_top_pxl = (net_theta_top - theta_min) * image2d_h / (theta_max - theta_min)
        r_bottom = mt.sqrt(
            (fld_l / 2 - plyrs_loc[plyr_num - 1, 1]) ** 2 + (x_real_pos - plyrs_loc[plyr_num - 1, 0]) ** 2 + (
                        net_h / 2 - plyr_h) ** 2)
        net_theta_bottom = mt.acos((net_h / 2 - plyr_h) / r_bottom)
        net_theta_bottom_pxl = (net_theta_bottom - theta_min) * image2d_h / (theta_max - theta_min)
        net_theta_arr = np.floor(np.linspace(net_theta_top_pxl, net_theta_bottom_pxl, net_lines_N))
        for j in range(0, net_lines_N):
            if (net_theta_arr[j] > 1) and (net_theta_arr[j] < image2d_h):
                image2d[int(net_theta_arr[j]) - 1: int(net_theta_arr[j]) + 1, x] = 1

        if np.max(x == net_phi_arr_pxl):
            up_p = int(mt.floor(max(net_theta_top_pxl, 1)))
            down_p = int(np.floor(np.min([net_theta_bottom_pxl, image2d_h])))
            image2d[up_p:down_p, x] = 1

    left_bottom_corner_y = plyrs_loc[plyr_num - 1, 1] + (fld_w - plyrs_loc[plyr_num - 1, 0]) * mt.tan(phi_min)
    if left_bottom_corner_y < fld_l:
        left_line_phi_min_pxl = 1
        left_line_phi_max = mt.atan2(fld_l - plyrs_loc[plyr_num - 1, 1], fld_w - plyrs_loc[plyr_num - 1, 0])
        left_line_phi_max_pxl = int(
            np.max([np.floor((left_line_phi_max - phi_min) * image2d_w / (phi_max - phi_min)), 1]))
        for phi_pxl in range(left_line_phi_min_pxl, left_line_phi_max_pxl):
            phi = phi_pxl * (phi_max - phi_min) / image2d_w + phi_min
            dot_y_loc = plyrs_loc[plyr_num - 1, 1] + (fld_w - plyrs_loc[plyr_num - 1, 0]) * mt.tan(phi)
            dot_r = mt.sqrt(np.sum((plyrs_loc[plyr_num - 1, :] - [fld_w, dot_y_loc]) ** 2) + plyr_h ** 2)
            theta = mt.acos(-plyr_h / dot_r)
            theta_pxl = int(np.floor((theta - theta_min) * image2d_h / (theta_max - theta_min)))
            if (theta_pxl > 1) and (theta_pxl < image2d_h - 1):
                image2d[theta_pxl - 1: theta_pxl + 1, phi_pxl] = 1

    right_bottom_corner_y = plyrs_loc[plyr_num - 1, 1] + (0 - plyrs_loc[plyr_num - 1, 0]) * mt.tan(phi_max)
    if right_bottom_corner_y < fld_l:
        right_line_phi_max_pxl = int(image2d_w)
        right_line_phi_min = mt.atan2(fld_l - plyrs_loc[plyr_num - 1, 1], 0 - plyrs_loc[plyr_num - 1, 0])
        right_line_phi_min_pxl = int(
            np.min([np.floor((right_line_phi_min - phi_min) * image2d_w / (phi_max - phi_min)), image2d_w]))
        for phi_pxl in range(right_line_phi_min_pxl, right_line_phi_max_pxl):
            phi = phi_pxl * (phi_max - phi_min) / image2d_w + phi_min
            dot_y_loc = plyrs_loc[plyr_num - 1, 1] + (0 - plyrs_loc[plyr_num - 1, 0]) * mt.tan(phi)
            dot_r = mt.sqrt(np.sum((plyrs_loc[plyr_num - 1, :] - [0, dot_y_loc]) ** 2) + plyr_h ** 2)
            theta = mt.acos(-plyr_h / dot_r)
            theta_pxl = int(np.floor((theta - theta_min) * image2d_h / (theta_max - theta_min)))
            if (theta_pxl > 1) and (theta_pxl < image2d_h):
                image2d[theta_pxl - 1: theta_pxl + 1, phi_pxl] = 1

    right_upper_corner_phi = np.max(
        [mt.atan2(fld_l - plyrs_loc[plyr_num - 1, 1], fld_w - plyrs_loc[plyr_num - 1, 0]), phi_min])
    right_upper_corner_phi_pxl = int(
        np.max([np.floor((right_upper_corner_phi - phi_min) * image2d_w / (phi_max - phi_min)), 1]))
    left_upper_corner_phi = np.min(
        [mt.atan2(fld_l - plyrs_loc[plyr_num - 1, 1], 0 - plyrs_loc[plyr_num - 1, 0]), phi_max])
    left_upper_corner_phi_pxl = int(
        np.min([np.floor((left_upper_corner_phi - phi_min) * image2d_w / (phi_max - phi_min)), image2d_w]))
    for phi_pxl in range(right_upper_corner_phi_pxl, left_upper_corner_phi_pxl):
        phi = phi_pxl * (phi_max - phi_min) / image2d_w + phi_min
        if phi == mt.pi / 2:
            dot_x_loc = plyrs_loc[plyr_num - 1, 0]
        else:
            dot_x_loc = plyrs_loc[plyr_num - 1, 0] + (fld_l - plyrs_loc[plyr_num - 1, 1]) / mt.tan(phi)
        dot_r = mt.sqrt(np.sum((plyrs_loc[plyr_num - 1, :] - [dot_x_loc, fld_l]) ** 2) + plyr_h ** 2)
        theta = mt.acos(-plyr_h / dot_r)
        theta_pxl = int(np.floor((theta - theta_min) * image2d_h / (theta_max - theta_min)))
        if (theta_pxl > 1) and (theta_pxl < image2d_h - 1):
            image2d[theta_pxl - 1: theta_pxl + 1, phi_pxl] = 1

    image2d = np.fliplr(image2d)
    return image2d, ball_plyr_dist, ball_theta, ball_phi, ball_in_view


def distances(team_number):
    dist_vec = np.zeros((1, N_plyrs))
    if team_number == 1:
        for plyr in range(0, N_plyrs):
            dist_vec[0, plyr] = dist(plyrs_loc[plyr, 0:2], [xf, yf])
    if team_number == 2:
        for plyr in range(N_plyrs, 2*N_plyrs):
            dist_vec[0, plyr-N_plyrs] = dist(plyrs_loc[plyr, 0:2], [xf, yf])
    return dist_vec


def distances_4_score(team_number):
    dist_vec = np.zeros((1, N_plyrs))
    if team_number == 1:
        for plyr in range(0, N_plyrs):
            dist_vec[0, plyr] = dist(plyrs_loc[plyr, 0:2], ball_loc[0, 0:2])
    if team_number == 2:
        for plyr in range(N_plyrs, 2*N_plyrs):
            dist_vec[0, plyr-N_plyrs] = dist(plyrs_loc[plyr, 0:2], ball_loc[0, 0:2])
    return dist_vec


def dist(v1, v2):
    distance = mt.sqrt((v2[0]-v1[0])**2+(v2[1]-v1[1])**2)
    return distance


# Parameters
pi = mt.pi
N_plyrs = 5  # number of players for each team
pxl_per_meter = 10
time_unit_per_sec = 20
plyr_vel = 2.8 * pxl_per_meter / time_unit_per_sec # average player velocity (m/s)
plyr_h = 1.8 * pxl_per_meter  # player height
plyr_w = 0.2 * pxl_per_meter  # player width (the player is a square in x-y plane)
net_h = 2.2 * pxl_per_meter  # net height
fld_w = 12 * pxl_per_meter  # field width
fld_l = 18 * pxl_per_meter  # field length
fld_h = 7 * pxl_per_meter  # field height
hit_pwr_max = 30 * pxl_per_meter / time_unit_per_sec  # maximal hit velocity (meters per sec)
top_plyr_hit = 0.5  # percentage from the upper part of the player that can make the hit
ball_r = 0.5 * pxl_per_meter  # ball radius
g = -10 * pxl_per_meter / (time_unit_per_sec ** 2)  # gravitational acceleration (meters per sec)
phi_min = mt.pi / 6  # the minimal azimuthal angle seen by the player
phi_max = 5 * mt.pi / 6  # the maximal azimuthal angle seen by the player
theta_min = 0  # the minimal longitude angle seen by the player
theta_max = mt.pi  # the maximal longitude angle seen by the player
image2d_h = 500  # height of the 2d image sent to the player
image2d_w = 1500

net_lines_N = 5
net_columns_N = 10

bounce_back_amp = 15  # amplitude of bounding back from a collision
rotate_matrix = np.array([(0, 1, 0, 0), (1, 0, 0, 0), (0, 0, 0, 1), (0, 0, 1, 0)])
net_thk = 0.04 * pxl_per_meter  # half of the net thickness
plyr_rad_pxl_limit = 5  # the maximum of half width of another player


"""
Zone 4: Tournament Scoring.
"""

score_success_multiplier = 10   # when a successful hit is made, the
#                                 player who hit the ball, will get a score
#                                 of the nearest player distance times this
#                                 number.
score_success_team_member = 40  # when a successful hit is made, the other
#                                 players from the winning team, will get
#                                 this score.
score_ball_hits_own_side = -2   # when someone hits the ball, but it hits
#                                 in the same side of the player who hit.
score_ball_hits_out = -3        # when the ball hits outside the field, the
#                                 player who hit the ball gets this score times
#                                 the height of the ball on the border.
score_mistake_other_team = 0    # when someone makes a mistake (ex: ball hits
#                                 out side the field), the other team gets
#                                 this score each.
score_ball_hits_net = -10       # when the ball hits the net or goes under it.
score_punish_losing_point = -20 # when the other team makes a successful hit, the losing one will have
#                                 negative score relative to the distance from the falling point.


"""
Zone 5: Game Management.
"""

plyrs_loc = np.zeros((N_plyrs * 2, 2))  # the x,y of all players
ball_loc = np.zeros((1, 3))  # the x,y,z of the ball
ball_vel = np.zeros((1, 3))  # the ball's x,y,z velocity

move = np.zeros((N_plyrs * 2, 1))
hit_pwr = np.ones((N_plyrs * 2, 1)) * 5
hit_theta = np.ones((N_plyrs * 2, 1)) * mt.pi / 4
hit_phi = np.ones((N_plyrs * 2, 1)) * mt.pi / 2
score_count = np.zeros((N_plyrs * 2 + 1, 1))  # counts the players score (one extra player
#                                               to get the points of the balls which hit
#                                               the floor at the first hit).
who_hit = N_plyrs * 2

next_game = 1  # which team starts this game
[plyrs_loc, ball_loc, ball_vel, xf, yf] = initiate_game(next_game, plyrs_loc, 1)

image_2d_top = np.zeros((fld_w, fld_l))
N_steps = int(2e4)  # number of time intervals

reason = ''
if ShowBoard:
    f1 = plt.figure(1, figsize=(7, 7.5))
    ax1 = f1.add_subplot(111)
if ShowPlayerView:
    f2d = plt.figure(2, figsize=(20, 10))
    ax2d = f2d.add_subplot(111)

for step in range(1, N_steps):
    ball_loc = np.array(ball_loc)

    # student's player
    student_image2d, ball_plyr_dist, ball_theta, ball_phi, ball_in_view = make_2d_image(plyrs_loc, ball_loc, 1)

    [move[0], hit_pwr[0], hit_theta[0], hit_phi[0]] = \
        student_player(student_image2d, ball_in_view)
    # first team
    for p in range(1, N_plyrs):
        [move[p], hit_pwr[p], hit_theta[p], hit_phi[p]] = \
            team1_turn(0, ball_loc, ball_vel, plyrs_loc[p, :], p)
    # second team
    for p in range(N_plyrs, N_plyrs * 2):
        [move[p], hit_pwr[p], hit_theta[p], hit_phi[p]] = \
            team2_turn(0, ball_loc, ball_vel, plyrs_loc[p, :], p)

    if ShowPlayerView:
        ns = plyrs_loc.shape + np.array((1, 0))
        field = np.zeros(ns)
        field[0:2 * N_plyrs, :] = plyrs_loc
        field[-1, :] = (-fld_w * 0.5, fld_l * 0.45)  # make a special reduced image for the learning stage        
        
        #field_image, ball_plyr_dist, ball_theta, ball_phi, ball_in_view = make_2d_image(field, ball_loc, 1)
        field_image, _, _, _, _ = make_2d_image(field, ball_loc, 1)
        
        ax2d.imshow(field_image, cmap='binary')
    
        if SavePlayerView and ball_in_view:
            f2d.savefig('ball_loc_step_{}_dist_{}_theta_{}_phi_{}_.png'.format(step,ball_plyr_dist, ball_theta, ball_phi))
    
    if ShowBoard:
        ax1.plot([fld_w, 0], [0, 0], 'k', lw=2)
        ax1.plot([0, 0], [fld_l, 0], 'k', lw=2)
        ax1.plot([fld_w, 0], [fld_l, fld_l], 'k', lw=2)
        ax1.plot([fld_w, fld_w], [0, fld_l], 'k', lw=2)
        ax1.plot([0, fld_w], [fld_l / 2, fld_l / 2], 'b', lw=3)
        ax1.text(fld_w * 1.05, fld_l / 4, 'step %i \n\n ball location \nx=%i \ny=%i \nz=%i'
                % (step, ball_loc[0, 0], ball_loc[0, 1], ball_loc[0, 2]), multialignment='center')
        ax1.text(fld_w * 1.1, fld_l / 2, 'my score: %s' % (score_count[0]), multialignment='center')
        #ax1.text(fld_w * 1.1, fld_l / 2, score_count, multialignment='center')
        ax1.text(fld_w * 1.1, fld_l , 'reason: %s' % (reason), multialignment='center')
        ax1.set_aspect('equal')

        for p in range(0, 2 * N_plyrs):
            ax1.scatter(plyrs_loc[p, 0], plyrs_loc[p, 1], s=100)
            ax1.text(plyrs_loc[p, 0], plyrs_loc[p, 1], ' %i' % (p + 1))

        ax1.scatter(ball_loc[0, 0], ball_loc[0, 1], s=200 * np.max((ball_loc[0, 2], 0.0000000001)) / fld_h, c='g')
        ax1.text(ball_loc[0, 0], ball_loc[0, 1], 'ball')

    plt.pause(0.01)
    ax1.clear()
    ball_loc, ball_vel, who_hit, next_game, score, xf, yf, reason = \
        advance_ball(ball_loc, ball_vel, plyrs_loc, hit_pwr, hit_theta, hit_phi, who_hit, xf, yf)

    if not next_game:  # if the ball didn't hit the floor/net/outside the edges
        # advances the players
        [plyrs_loc, plyrs_collide] = advance_plyrs(plyrs_loc, move)
    else:
        # initiates the game again (same location for the players, winning
        # team starts)
        score_count[who_hit] = score_count[who_hit] + score
        if who_hit <= N_plyrs:  # if in the first team
            if score > 0:  # if did something good
                for j in range(0, N_plyrs):  # that player's team members get points
                    if j == who_hit:
                        continue
                    score_count[j] = score_count[j] + score_success_team_member
            elif score < 0:  # did something bad
                for j in range(N_plyrs, N_plyrs * 2):  # the other team's team members get points
                    score_count[j] = score_count[j] + score_mistake_other_team
        else:
            if score > 0:  # if did something good
                for j in range(N_plyrs, N_plyrs * 2):  # that player's team members get points
                    if j == who_hit:
                        continue
                    score_count[j] = score_count[j] + score_success_team_member
            elif score < 0:  # did something bad
                for j in range(0, N_plyrs):  # the other team's team members get points
                    score_count[j] = score_count[j] + score_mistake_other_team

        [plyrs_loc, ball_loc, ball_vel, xf, yf] = initiate_game(next_game, plyrs_loc, 0)  # starts a new game
        who_hit = N_plyrs * 2
