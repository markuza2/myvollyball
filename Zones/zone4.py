

"""
Zone 4: Tournament Scoring.
"""

score_success_multiplier = 10   # when a successful hit is made, the
#                                 player who hit the ball, will get a score
#                                 of the nearest player distance times this
#                                 number.
score_success_team_member = 40  # when a successful hit is made, the other
#                                 players from the winning team, will get
#                                 this score.
score_ball_hits_own_side = -2   # when someone hits the ball, but it hits
#                                 in the same side of the player who hit.
score_ball_hits_out = -3        # when the ball hits outside the field, the
#                                 player who hit the ball gets this score times
#                                 the height of the ball on the border.
score_mistake_other_team = 0    # when someone makes a mistake (ex: ball hits
#                                 out side the field), the other team gets
#                                 this score each.
score_ball_hits_net = -10       # when the ball hits the net or goes under it.
score_punish_losing_point = -20 # when the other team makes a successful hit, the losing one will have
#                                 negative score relative to the distance from the falling point.
