
Zone 5: Game Management.
"""

plyrs_loc = np.zeros((N_plyrs * 2, 2))  # the x,y of all players
ball_loc = np.zeros((1, 3))  # the x,y,z of the ball
ball_vel = np.zeros((1, 3))  # the ball's x,y,z velocity

move = np.zeros((N_plyrs * 2, 1))
hit_pwr = np.ones((N_plyrs * 2, 1)) * 5
hit_theta = np.ones((N_plyrs * 2, 1)) * mt.pi / 4
hit_phi = np.ones((N_plyrs * 2, 1)) * mt.pi / 2
score_count = np.zeros((N_plyrs * 2 + 1, 1))  # counts the players score (one extra player
#                                               to get the points of the balls which hit
#                                               the floor at the first hit).
who_hit = N_plyrs * 2

next_game = 1  # which team starts this game
[plyrs_loc, ball_loc, ball_vel, xf, yf] = initiate_game(next_game, plyrs_loc, 1)

image_2d_top = np.zeros((fld_w, fld_l))
N_steps = int(2e4)  # number of time intervals

reason = ''
if ShowBoard:
    f1 = plt.figure(1, figsize=(7, 7.5))
    ax1 = f1.add_subplot(111)
if ShowPlayerView:
    f2d = plt.figure(2, figsize=(20, 10))
    ax2d = f2d.add_subplot(111)

for step in range(1, N_steps):
    ball_loc = np.array(ball_loc)

    # student's player
    student_image2d, ball_plyr_dist, ball_theta, ball_phi, ball_in_view = make_2d_image(plyrs_loc, ball_loc, 1)

    [move[0], hit_pwr[0], hit_theta[0], hit_phi[0]] = \
        student_player(student_image2d, ball_in_view)
    # first team
    for p in range(1, N_plyrs):
        [move[p], hit_pwr[p], hit_theta[p], hit_phi[p]] = \
            team1_turn(0, ball_loc, ball_vel, plyrs_loc[p, :], p)
    # second team
    for p in range(N_plyrs, N_plyrs * 2):
        [move[p], hit_pwr[p], hit_theta[p], hit_phi[p]] = \
            team2_turn(0, ball_loc, ball_vel, plyrs_loc[p, :], p)

    if ShowPlayerView:
        ns = plyrs_loc.shape + np.array((1, 0))
        field = np.zeros(ns)
        field[0:2 * N_plyrs, :] = plyrs_loc
        field[-1, :] = (-fld_w * 0.5, fld_l * 0.45)  # make a special reduced image for the learning stage        
        
        #field_image, ball_plyr_dist, ball_theta, ball_phi, ball_in_view = make_2d_image(field, ball_loc, 1)
        field_image, _, _, _, _ = make_2d_image(field, ball_loc, 1)
        
        ax2d.imshow(field_image, cmap='binary')
    
        if SavePlayerView and ball_in_view:
            f2d.savefig('ball_loc_step_{}_dist_{}_theta_{}_phi_{}_.png'.format(step,ball_plyr_dist, ball_theta, ball_phi))
    
    if ShowBoard:
        ax1.plot([fld_w, 0], [0, 0], 'k', lw=2)
        ax1.plot([0, 0], [fld_l, 0], 'k', lw=2)
        ax1.plot([fld_w, 0], [fld_l, fld_l], 'k', lw=2)
        ax1.plot([fld_w, fld_w], [0, fld_l], 'k', lw=2)
        ax1.plot([0, fld_w], [fld_l / 2, fld_l / 2], 'b', lw=3)
        ax1.text(fld_w * 1.05, fld_l / 4, 'step %i \n\n ball location \nx=%i \ny=%i \nz=%i'
                % (step, ball_loc[0, 0], ball_loc[0, 1], ball_loc[0, 2]), multialignment='center')
        ax1.text(fld_w * 1.1, fld_l / 2, 'my score: %s' % (score_count[0]), multialignment='center')
        #ax1.text(fld_w * 1.1, fld_l / 2, score_count, multialignment='center')
        ax1.text(fld_w * 1.1, fld_l , 'reason: %s' % (reason), multialignment='center')
        ax1.set_aspect('equal')

        for p in range(0, 2 * N_plyrs):
            ax1.scatter(plyrs_loc[p, 0], plyrs_loc[p, 1], s=100)
            ax1.text(plyrs_loc[p, 0], plyrs_loc[p, 1], ' %i' % (p + 1))

        ax1.scatter(ball_loc[0, 0], ball_loc[0, 1], s=200 * np.max((ball_loc[0, 2], 0.0000000001)) / fld_h, c='g')
        ax1.text(ball_loc[0, 0], ball_loc[0, 1], 'ball')

    plt.pause(0.01)
    ax1.clear()
    ball_loc, ball_vel, who_hit, next_game, score, xf, yf, reason = \
        advance_ball(ball_loc, ball_vel, plyrs_loc, hit_pwr, hit_theta, hit_phi, who_hit, xf, yf)

    if not next_game:  # if the ball didn't hit the floor/net/outside the edges
        # advances the players
        [plyrs_loc, plyrs_collide] = advance_plyrs(plyrs_loc, move)
    else:
        # initiates the game again (same location for the players, winning
        # team starts)
        score_count[who_hit] = score_count[who_hit] + score
        if who_hit <= N_plyrs:  # if in the first team
            if score > 0:  # if did something good
                for j in range(0, N_plyrs):  # that player's team members get points
                    if j == who_hit:
                        continue
                    score_count[j] = score_count[j] + score_success_team_member
            elif score < 0:  # did something bad
                for j in range(N_plyrs, N_plyrs * 2):  # the other team's team members get points
                    score_count[j] = score_count[j] + score_mistake_other_team
        else:
            if score > 0:  # if did something good
                for j in range(N_plyrs, N_plyrs * 2):  # that player's team members get points
                    if j == who_hit:
                        continue
                    score_count[j] = score_count[j] + score_success_team_member
            elif score < 0:  # did something bad
                for j in range(0, N_plyrs):  # the other team's team members get points
                    score_count[j] = score_count[j] + score_mistake_other_team

        [plyrs_loc, ball_loc, ball_vel, xf, yf] = initiate_game(next_game, plyrs_loc, 0)  # starts a new game
        who_hit = N_plyrs * 2
