
"""
Zone 2: Student Zone. Start writing your answer here.
"""

# Below is an example of a simplified solution.

VirtualBallFollower = (0,0,0) # Global variable used to display the predicted ball position on the board.

# Size of the images that our model was trained by.
width = 400
height = 200

# Getting the trained model.
model = load_model('complete_model.h5') # Use the model you trained on colab and dowloaded to your disk, and place it in the directory of this program.

# Your artificial voleyball player.
def student_player(image, ball_in_view):
    global VirtualBallFollower
    if ball_in_view:
        if UseTrainedModel:
            image = cv2.resize(image,(width,height)) # Resizing to the same tensor size as used when learning (1,width,height,1).
            image = np.expand_dims(image,-1)  
            image = np.expand_dims(image, axis=0) 

            # Doing prediction using the trained model.
            prediction = model.predict(image)
            ball_distance, ball_theta, ball_phi = prediction[0]
            VirtualBallFollower = prediction[0] # Provision for displaying the predicted vector to ball.
            ball_theta = ball_theta/100.0 # Used for normalization as in the training.
            ball_phi = ball_phi/100.0 # User for normalization as in the training.
            
            # Call your unsupervised machine learning part here and calculate your game step and hit parameters...

            # Example: Random player (same as below).
            move = np.random.randint(1, 5, (1, 1)) - 1
            phi_range = 40
            phi_offset = 90
            hit_phi = np.random.random(1) * phi_range * 2 * pi / 180 + phi_offset * pi / 180 - phi_range * pi / 180
            hit_theta = pi / 4
            pwr_offset = hit_pwr_max * 0.7
            pwr_range = hit_pwr_max * 0.3
            hit_pwr = np.random.random(1) * pwr_range + pwr_offset - pwr_range/2
        else:
            # Example: Random player (same as above).
            move = np.random.randint(1, 5, (1, 1)) - 1
            phi_range = 40
            phi_offset = 90
            hit_phi = np.random.random(1) * phi_range * 2 * pi / 180 + phi_offset * pi / 180 - phi_range * pi / 180
            hit_theta = pi / 4
            pwr_offset = hit_pwr_max * 0.7
            pwr_range = hit_pwr_max * 0.3
            hit_pwr = np.random.random(1) * pwr_range + pwr_offset - pwr_range/2
    else: # Doing something if the ball is not in the view.
        # For the example we're not really doing anything...
        move = np.random.randint(1, 5, (1, 1)) - 2
        hit_pwr = 0
        hit_theta = 0
        hit_phi = 0

    # This is your answer.
    return [move, hit_pwr, hit_theta, hit_phi]

