
"""
Zone 1: Program Initialization.
"""
# Required Imports.
import numpy as np # Use 'pip install numpy' if not installed with your Python.
import numpy.matlib
import matplotlib.pyplot as plt # Use 'pip install matplotlib' if not installed with your Python.
import math as mt
import cv2
import json
from sklearn.externals.joblib import dump, load
from tensorflow.keras.layers import *
from tensorflow.keras.models import *
from tensorflow.keras.models import load_model

# Flags for different options while running the game.
ShowBoard = True # Showing Board View (called 2d view in the code).
ShowPlayerView = False # Showing view as seen by our player (the view given to the student artificial player).
SavePlayerView = False # Saving images of views as seen by player (saved to ./plots directory). These files should be used for training.
UseTrainedModel = False # Choosing if we use the trained model or a random player for the student artificial player.

# Starting the game with random positions for the players and the ball.
def initiate_game(start_team, plyrs_loc, new_match):
    if new_match:
        plyrs_loc[0:N_plyrs * 2, 0] = np.random.randint(mt.floor(fld_w * 0.05), mt.floor(fld_w * 0.95),
                                                        size=[1, N_plyrs * 2])
        plyrs_loc[0:N_plyrs, 1] = np.random.randint(mt.floor(fld_l * 0.05), mt.floor(fld_l * 0.45), size=[1, N_plyrs])
        plyrs_loc[N_plyrs:N_plyrs * 2, 1] = np.random.randint(mt.floor(fld_l * 0.55), mt.floor(fld_l * 0.95),
                                                              [1, N_plyrs])

    ball_loc = np.zeros((1, 3))
    ball_loc[0, 0] = np.random.randint(mt.floor(0.1 * fld_w), mt.floor(0.9 * fld_w))
    ball_loc[0, 1] = np.random.randint(mt.floor(((start_team - 1) * 0.5 + 0.1) * fld_l),
                                       mt.floor(((start_team - 1) * 0.5 + 0.2) * fld_l))
    ball_loc[0, 2] = plyr_h

    ball_vel = np.zeros((1, 3))
    v0 = hit_pwr_max * 0.6 * pxl_per_meter / time_unit_per_sec  # absolute value of the ball's velocity
    alpha = 45 * (mt.pi / 180)  # degrees over the horizon of the velocity
    ball_vel[0, 0] = 0
    ball_vel[0, 1] = v0 * mt.cos(alpha) * (-2 * start_team + 3)
    ball_vel[0, 2] = v0 * mt.sin(alpha)

    t_a = g / 2
    t_b = ball_vel[0, 2]
    t_c = ball_loc[0, 2] - plyr_h
    temp = t_b ** 2 - 4 * t_a * t_c
    if temp < 0:
        temp = 0
    t1 = (-t_b + mt.sqrt(temp)) / (2 * t_a)
    t2 = (-t_b - mt.sqrt(temp)) / (2 * t_a)
    t = max(t1, t2)
    xf = ball_loc[0, 0] + ball_vel[0, 0] * t
    yf = ball_loc[0, 1] + ball_vel[0, 1] * t

    return [plyrs_loc, ball_loc, ball_vel, xf, yf]

